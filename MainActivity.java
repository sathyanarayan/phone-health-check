package com.example.sathyaam.applicationone;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Vibrator;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.bluetooth.BluetoothAdapter;
import android.speech.tts.TextToSpeech;
import android.widget.Toast;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.content.pm.PackageManager;

import java.util.Locale;

import static android.net.wifi.SupplicantState.INVALID;

public class MainActivity extends AppCompatActivity  {

    EditText edittext1;
    Button button,button2,button3;
    int mark=0;
    IntentFilter intentfilter;
    int status;
    Vibrator vibrator;
    BluetoothAdapter blth;
    TextToSpeech t2;
    ImageView mImageView,mImageView2,mImageView3,mImageView5,mImageView6,mImageView7,mImageView8,mImageView9;
    ImageButton mImageView4;
    Drawable mIcon,mIcon2,mIcon3,mIcon4,mIcon5,mIcon6,mIcon7,mIcon8,mIcon9;
    public PackageManager packageManager;
    LinearLayout cameraview,cameraview2;
    Uri file;
    CameraPreview mPreview;
    Camera c,d = null;
    KeyEvent event;
    GPSTracker gps;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edittext1=(EditText)findViewById(R.id.etext1);
        button = (Button) findViewById(R.id.button1);
        button2= (Button) findViewById(R.id.button2);
        button3= (Button) findViewById(R.id.button3);
        vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
        blth = BluetoothAdapter.getDefaultAdapter();
        intentfilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        mImageView=(ImageView)findViewById(R.id.imageView);
        mImageView2=(ImageView)findViewById(R.id.imageView2);
        mImageView3=(ImageView)findViewById(R.id.imageView3);
        mImageView4=(ImageButton)findViewById(R.id.imageButton);
        mImageView5=(ImageView)findViewById(R.id.imageView5);
        mImageView6=(ImageView)findViewById(R.id.imageView4);
        mImageView7=(ImageView)findViewById(R.id.imageView6);
        mImageView8=(ImageView)findViewById(R.id.imageView10);
        mImageView9=(ImageView)findViewById(R.id.imageView11);
        cameraview = (LinearLayout) findViewById(R.id.linear);
        cameraview2 = (LinearLayout) findViewById(R.id.linear2);
        packageManager = getApplicationContext().getPackageManager();
        mIcon = ContextCompat.getDrawable(getApplicationContext(), R.drawable.batterytwo);
        mIcon.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        mImageView.setImageDrawable(mIcon);
        mIcon2 = ContextCompat.getDrawable(getApplicationContext(), R.drawable.vibrator);
        mIcon2.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        mImageView2.setImageDrawable(mIcon2);
        mIcon3 = ContextCompat.getDrawable(getApplicationContext(), R.drawable.audio);
        mIcon3.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        mImageView3.setImageDrawable(mIcon3);
        mIcon4 = ContextCompat.getDrawable(getApplicationContext(), R.drawable.phone);
        mIcon4.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.white), PorterDuff.Mode.MULTIPLY);
        mImageView4.setImageDrawable(mIcon4);
        mIcon5 = ContextCompat.getDrawable(getApplicationContext(), R.drawable.bluetooth);
        mIcon5.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        mImageView5.setImageDrawable(mIcon5);
        mIcon6 = ContextCompat.getDrawable(getApplicationContext(), R.drawable.camera);
        mIcon6.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        mImageView6.setImageDrawable(mIcon6);
        mIcon7 = ContextCompat.getDrawable(getApplicationContext(), R.drawable.wifi);
        mIcon7.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        mImageView7.setImageDrawable(mIcon7);
        mIcon8 = ContextCompat.getDrawable(getApplicationContext(), R.drawable.button);
        mIcon8.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        mImageView8.setImageDrawable(mIcon8);
        mIcon9 = ContextCompat.getDrawable(getApplicationContext(), R.drawable.gps);
        mIcon9.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        mImageView9.setImageDrawable(mIcon9);


        t2=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    t2.setLanguage(Locale.US);
                }
            }
        });
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                MainActivity.this.registerReceiver(broadcastreceiver, intentfilter);



                if(vibrator.hasVibrator()){

                    vibrator.vibrate(400);
                    mIcon2.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.green), PorterDuff.Mode.MULTIPLY);
                    mImageView2.setImageDrawable(mIcon2);
                    mark=mark+1;

                }
                else {
                    ;}
                WifiManager wifi = (WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                SupplicantState supState;
                WifiInfo wifiInfo;
                if(wifi.isWifiEnabled()) {
                    wifiInfo = wifi.getConnectionInfo();
                    supState = wifiInfo.getSupplicantState();
                    if (supState == INVALID)
                        Toast.makeText(getApplicationContext(), "WIFI not working", Toast.LENGTH_LONG).show();
                    else
                    {Toast.makeText(getApplicationContext(), "WIFI is working", Toast.LENGTH_LONG).show();
                        mIcon7.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.green), PorterDuff.Mode.MULTIPLY);
                        mImageView7.setImageDrawable(mIcon7);
                        mark=mark+1;}
                }
                else{
                    wifi.setWifiEnabled(true);
                    Toast.makeText(getApplicationContext(),"WIFI is enabling",Toast.LENGTH_LONG).show();
                    wifiInfo = wifi.getConnectionInfo();
                    supState = wifiInfo.getSupplicantState();
                    if (supState == INVALID)
                        Toast.makeText(getApplicationContext(), "WIFI not working", Toast.LENGTH_LONG).show();
                    else
                    {Toast.makeText(getApplicationContext(), "WIFI is working", Toast.LENGTH_LONG).show();
                        mIcon7.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.green), PorterDuff.Mode.MULTIPLY);
                        mImageView7.setImageDrawable(mIcon7);
                        mark=mark+1;}
                }
                gps = new GPSTracker(MainActivity.this);

                // check if GPS enabled
                if(gps.canGetLocation()){

                    double latitude = gps.getLatitude();
                    double longitude = gps.getLongitude();

                    // \n is for new line
                    Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                    mIcon9.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.green), PorterDuff.Mode.MULTIPLY);
                    mImageView9.setImageDrawable(mIcon9);
                    mark=mark+1;
                }else{
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gps.showSettingsAlert();
                }


                try{
                    AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                    alertDialog.setTitle("Alert");
                    alertDialog.setMessage("please type the word system pronounces");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    dialog.dismiss();
                                    t2.speak("please do type the word being pronounced amazon",TextToSpeech.QUEUE_FLUSH,null);
                                    String value = (String) edittext1.getText().toString();

                                }
                            });
                    alertDialog.show();




                }
                catch (Exception e) {
                    e.printStackTrace();
                }

               try{

                if(blth.isEnabled())
                    { Toast.makeText(getApplicationContext(), "Already on" ,Toast.LENGTH_LONG).show();
                        blth.disable();
                    }

                else{
                        Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(turnOn, 0);
                        Toast.makeText(getApplicationContext(), "Turning on",Toast.LENGTH_LONG).show();
                        blth.disable();
                    }
                   mIcon5.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.green), PorterDuff.Mode.MULTIPLY);
                   mImageView5.setImageDrawable(mIcon5);
                   mark=mark+1;}
                    catch (Exception e){;}


                if (packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
                    //yes
                    Toast.makeText(getApplicationContext(),"this device have camera",Toast.LENGTH_LONG).show();

                    try {

                        int totcamera=Camera.getNumberOfCameras();
                        CameraInfo info = new CameraInfo();
                        for(int i=0;i<totcamera;i++)
                        {
                            Camera.getCameraInfo(i, info);
                            if(info.facing == CameraInfo.CAMERA_FACING_FRONT)
                            {Toast.makeText(getApplicationContext(),"checking back camera",Toast.LENGTH_LONG).show();
                                c = Camera.open(i);
                                c.setDisplayOrientation(90);
                                mPreview = new CameraPreview(getApplicationContext(),c);
                                cameraview.addView(mPreview);
                                continue;
                            }
                            else if(info.facing == CameraInfo.CAMERA_FACING_BACK)
                            {Toast.makeText(getApplicationContext(),"checking front camera",Toast.LENGTH_LONG).show();
                                d = Camera.open(i);
                                d.setDisplayOrientation(90);
                                mPreview = new CameraPreview(getApplicationContext(),d);
                                cameraview2.addView(mPreview);

                                continue;
                            }
                            else{continue;}
                        }
                        //c = Camera.open(0); // attempt to get a Camera instance
                        //Toast.makeText(getApplicationContext(),"got permission",Toast.LENGTH_LONG).show();
                        //mPreview = new CameraPreview(this,c);
                        //cameraview.addView(mPreview);
                        mIcon6.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.green), PorterDuff.Mode.MULTIPLY);
                        mImageView6.setImageDrawable(mIcon6);
                        mark=mark+1;

                    }
                    catch (Exception e){
                        // Camera is not available (in use or does not exist)
                        Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();}

                }
                else{
                    //no
                    Toast.makeText(getApplicationContext(),"this device have no camera",Toast.LENGTH_LONG).show();
                }


            }
        });
        button2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String value = (String) edittext1.getText().toString();
                boolean k = value.equalsIgnoreCase("amazon");
                if (k)
                {Toast.makeText(getApplicationContext(), "correct" ,Toast.LENGTH_LONG).show();
                    mIcon3.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.green), PorterDuff.Mode.MULTIPLY);
                    mImageView3.setImageDrawable(mIcon3);
                    mark=mark+1;}
                else {Toast.makeText(getApplicationContext(), "INCORRECT ENTRY PLEASE TRY AGAIN" ,Toast.LENGTH_LONG).show();}


            }
        });
        mImageView4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                final Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:9895935659"));


                if (ActivityCompat.checkSelfPermission(MainActivity.this,
                        Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(callIntent);
                TelephonyManager telephonyManager =(TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);


                PhoneStateListener callStateListener = new PhoneStateListener() {
                    public void onCallStateChanged(int state, String incomingNumber)
                    {
                        if(state==TelephonyManager.CALL_STATE_RINGING){
                            Toast.makeText(getApplicationContext(),"Phone Is Riging",
                                    Toast.LENGTH_LONG).show();
                            mIcon4.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.green), PorterDuff.Mode.MULTIPLY);
                            mImageView4.setImageDrawable(mIcon4);
                            mark=mark+1;

                        }
                        if(state==TelephonyManager.CALL_STATE_OFFHOOK){
                            Toast.makeText(getApplicationContext(),"Phone is Currently in A call",
                                    Toast.LENGTH_LONG).show();
                            mIcon4.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.green), PorterDuff.Mode.MULTIPLY);
                            mImageView4.setImageDrawable(mIcon4);

                        }

                        if(state==TelephonyManager.CALL_STATE_IDLE){
                            Toast.makeText(getApplicationContext(),"phone is neither ringing nor in a call",
                                    Toast.LENGTH_LONG).show();

                        }
                    }
                };
                telephonyManager.listen(callStateListener,PhoneStateListener.LISTEN_CALL_STATE);

            }


        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, ScreenTest.class);
                i.putExtra("score",mark);
                startActivity(i);
            }
        });

    }



    BroadcastReceiver broadcastreceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            status = intent.getIntExtra(BatteryManager.EXTRA_HEALTH, 0);

            if (status == BatteryManager.BATTERY_HEALTH_COLD) {



            }
            if (status == BatteryManager.BATTERY_HEALTH_DEAD) {



            }
            if (status == BatteryManager.BATTERY_HEALTH_GOOD) {


                mIcon = ContextCompat.getDrawable(getApplicationContext(), R.drawable.batterytwo);
                mIcon.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.green), PorterDuff.Mode.MULTIPLY);
                mImageView.setImageDrawable(mIcon);
                mark=mark+1;


            }
            if (status == BatteryManager.BATTERY_HEALTH_OVERHEAT) {



            }
            if (status == BatteryManager.BATTERY_HEALTH_OVER_VOLTAGE) {


            }
            if (status == BatteryManager.BATTERY_HEALTH_UNKNOWN) {


            }
            if (status == BatteryManager.BATTERY_HEALTH_UNSPECIFIED_FAILURE) {



            }


        }
    };
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            Toast.makeText(getApplicationContext(),"button pressed",Toast.LENGTH_LONG).show();
            mIcon8.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.green), PorterDuff.Mode.MULTIPLY);
            mImageView8.setImageDrawable(mIcon8);
            mark=mark+1;
            return true;
        }
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            Toast.makeText(getApplicationContext(),"button pressed down ",Toast.LENGTH_LONG).show();
            mIcon8.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.green), PorterDuff.Mode.MULTIPLY);
            mImageView8.setImageDrawable(mIcon8);
            mark=mark+1;
            return true;
        }

        return false;
    }
}

